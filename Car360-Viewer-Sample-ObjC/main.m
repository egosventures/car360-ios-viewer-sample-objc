//
//  main.m
//  Car360-Viewer-Sample-ObjC
//
//  Created by Ross Chapman on 7/25/16.
//  Copyright © 2016 Egos Ventures. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
