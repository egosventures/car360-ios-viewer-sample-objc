//
//  ViewController.m
//  Car360-Viewer-Sample-ObjC
//
//  Created by Ross Chapman on 7/25/16.
//  Copyright © 2016 Egos Ventures. All rights reserved.
//

#import "ViewController.h"

@import Car360Viewer;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    OnlineViewerController *viewerController = [Car360Framework getViewerViewControllerForSpin:@"6fbae03d43aca902b3983ca85ce1025c" configurationHandler:^(OnlineViewerConfiguration * _Nonnull config) {
        config.resolution = OnlineViewerResolutionRes960x540;
    }];
    [self presentViewController:viewerController animated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
