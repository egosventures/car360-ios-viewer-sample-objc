//
//  AppDelegate.h
//  Car360-Viewer-Sample-ObjC
//
//  Created by Ross Chapman on 7/25/16.
//  Copyright © 2016 Egos Ventures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

